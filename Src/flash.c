// #include "stm32f0xx_hal.h"
// #include "stm32f0xx_hal_flash.h"
// #include "stm32_hal_legacy.h"
// #include "flash.h"
// #include "stm32f0xx_hal_flash_ex.h"
// #include "Trace.h"
// #define FLASH_ADDR_TOP            0x08004000            //STM32F030F4P6的flash大小只有16k
// //#define FLASH_TYPEERASE_PAGES     ((uint32_t)0x00)  /*!<Pages erase only*/
// //#define FLASH_TYPEERASE_MASSERASE ((uint32_t)0x01)  /*!<Flash mass erase activation*/

// //uint32_t writeFlashData = 0x55555555;
// //uint32_t addr = 0x08007000;
// void flash_write_data(uint32_t flash_addr, uint32_t write_flash_data)
// {
//     //1、解锁FLASH
//   HAL_FLASH_Unlock();

//     //2、擦除FLASH
//     //初始化FLASH_EraseInitTypeDef
//     FLASH_EraseInitTypeDef f;
//     f.TypeErase = FLASH_TYPEERASE_PAGES;
//     f.PageAddress = flash_addr;
//     f.NbPages = 1;
//     //设置PageError
//     uint32_t PageError = 0;
//     //调用擦除函数
//     HAL_FLASHEx_Erase(&f, &PageError);

//     //3、对FLASH烧写
//     HAL_FLASH_Program(TYPEPROGRAM_WORD, flash_addr, write_flash_data);

//     //4、锁住FLASH
//   HAL_FLASH_Lock();
// }

// //flash读取数据
// uint32_t flash_read_data(uint32_t flash_addr)
// {
//   uint32_t temp = *(__IO uint32_t*)(flash_addr);
//   return temp;
// //    printf("addr:0x%x, data:0x%x\r\n", flash_addr, temp);
// }

// #define OB_WRP_AllPages                (0x0000fffful) /*!< Write protection of all Sectors */

// void flash_protection(void)
// {
// /*    HAL_FLASH_OB_Unlock();
//   FLASH_OB_RDP_LevelConfig(OB_RDP_LEVEL_2);
//   FLASH_OB_EnableWRP(OB_WRP_AllPages);
//     HAL_FLASH_OB_Lock();*/
// /*  HAL_FLASH_OB_Unlock();
//   HAL_FLASH_Unlock();
//   OB->RDP = 0x00;
//   FLASH_OB_RDP_LevelConfig(OB_RDP_LEVEL_2);
//   FLASH_OB_EnableWRP(OB_WRP_AllPages);
//   HAL_FLASH_Lock();
//   HAL_FLASH_OB_Lock();*/

// }
// #define RDP_Key                  ((uint16_t)0x00A5u)
// #define CR_STRT_Set              ((uint32_t)0x00000040ul)
// #define CR_OPTER_Set             ((uint32_t)0x00000020ul)
// #define FLASH_KEY11               ((uint32_t)0x45670123ul)
// #define FLASH_KEY21               ((uint32_t)0xCDEF89ABul)
// #define CR_OPTPG_Reset           ((uint32_t)0x00001FEFul)
// #define CR_OPTER_Reset           ((uint32_t)0x00001FDFul)
// #define CR_OPTPG_Set             ((uint32_t)0x00000010ul)
// #define EraseTimeout             ((uint32_t)0x00000FFFul)
// typedef enum
// {
//   FLASH_BUSY1 = 1,
//   FLASH_ERROR_PG1,
//   FLASH_ERROR_WRP1,
//   FLASH_COMPLETE,
//   FLASH_TIMEOUT
// }FLASH_Status;
// FLASH_Status FLASH_ReadOutProtection(FunctionalState NewState)
// {
//   FLASH_Status status = FLASH_COMPLETE;
//   /* 检查参数 */
//   assert_param(IS_FUNCTIONAL_STATE(NewState));
//   status = FLASH_WaitForLastOperation(EraseTimeout);
// //      trace_printf("%d\n",status);
//   if(status == FLASH_COMPLETE)
//   {
//     /* 授权小信息块编程 */
//     FLASH->OPTKEYR = FLASH_KEY11;
//     FLASH->OPTKEYR = FLASH_KEY21;
//     FLASH->CR |= CR_OPTER_Set;
//     FLASH->CR |= CR_STRT_Set;
//     /* 等待最后一个操作完成 */
//     status = FLASH_WaitForLastOperation(EraseTimeout);
//  //   trace_printf("a\n");
//     if(status == FLASH_COMPLETE)
//     {
//       /* 如果擦除操作完成，失能 OPTER 位 */
//       FLASH->CR &= CR_OPTER_Reset;
//       /* 使能字节编程操作选项 */
//       FLASH->CR |= CR_OPTPG_Set;
//       if(NewState != DISABLE)
//       {
//         OB->RDP = 0x00;
//       }
//       else
//       {
//         OB->RDP = RDP_Key;
//       }
//       /* 等待最后一个操作完成 */
//       status = FLASH_WaitForLastOperation(EraseTimeout);
//       if(status != FLASH_TIMEOUT)
//       {
//         /* 如果编程操作完成，失能 OPTPG 位 */
//         FLASH->CR &= CR_OPTPG_Reset;
// //                  trace_printf("b\n");
//       }
//     }
//     else
//     {
//       if(status != FLASH_TIMEOUT)
//       {
//         /* 失能 OPTER 位 */
//         FLASH->CR &= CR_OPTER_Reset;
// //                  trace_printf("c\n");
//       }
//     }
//   }
//   /* 返回写保护操作状态 */
//   return status;
// }
// #define FLASH_ER_PRG_TIMEOUT         ((uint32_t)0x000B0000ul)
// FLASH_Status FLASH_OB_RDPConfig(uint8_t OB_RDP)
// {
// #if 0
//   FLASH_Status status = FLASH_COMPLETE;

//   /* Check the parameters */
//   assert_param(IS_OB_RDP_LEVEL(OB_RDP));
//   status = FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);

//   if(status == FLASH_COMPLETE)
//   {
//     FLASH->CR |= FLASH_CR_OPTER;
//     FLASH->CR |= FLASH_CR_STRT;

//     /* Wait for last operation to be completed */
//     status = FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);

//     if(status == FLASH_COMPLETE)
//     {
//       /* If the erase operation is completed, disable the OPTER Bit */
//       FLASH->CR &= ~FLASH_CR_OPTER;

//       /* Enable the Option Bytes Programming operation */
//       FLASH->CR |= FLASH_CR_OPTPG;

//       OB->RDP = OB_RDP;

//       /* Wait for last operation to be completed */
//       status = FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);

//       if(status != FLASH_TIMEOUT)
//       {
//         /* if the program operation is completed, disable the OPTPG Bit */
//         FLASH->CR &= ~FLASH_CR_OPTPG;
//       }
//     }
//     else
//     {
//       if(status != FLASH_TIMEOUT)
//       {
//         /* Disable the OPTER Bit */
//         FLASH->CR &= ~FLASH_CR_OPTER;
//       }
//     }
//   }
//   /* Return the protection operation Status */
//   return status;
// #else
//   FLASH_Status status = FLASH_COMPLETE;
//   uint16_t tmp1 = 0;
//   uint32_t tmp2 = 0;
//   /* Check the parameters */
//   assert_param(IS_OB_RDP_LEVEL(OB_RDP));
//   status = FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);

//   /* calculate the option byte to write */
// // tmp1=(uint8_t)(~(OB_RDP ));
//  tmp1 =(~((uint32_t)(OB_RDP )));
//  tmp2=(uint32_t)(((uint32_t)((uint32_t)(tmp1)<<16))|((uint32_t)OB_RDP));
// // if(status == FLASH_COMPLETE) {
//   OB->RDP = tmp2;  /* program read protection level */
// //  }

//   /* Wait for last operation to be completed */
//     status = FLASH_WaitForLastOperation(FLASH_ER_PRG_TIMEOUT);

//   /* Return the Read protection operation Status */
//   return status;
// #endif
// }




// void flash_play(void)
// {
// /*  FLASH_Status status1 = 0;
//     HAL_FLASH_OB_Unlock();
//   HAL_FLASH_Unlock();
//   status1 = FLASH_ReadOutProtection(ENABLE);
// //  trace_printf("%d\n",status1);
//     HAL_FLASH_Lock();
//   HAL_FLASH_OB_Lock();*/
//   FLASH_OBProgramInitTypeDef OBInit;

//   __HAL_FLASH_PREFETCH_BUFFER_DISABLE();

//   HAL_FLASHEx_OBGetConfig(&OBInit);
//   if(OBInit.RDPLevel == OB_RDP_LEVEL_0)
//   {
//     OBInit.OptionType = OPTIONBYTE_RDP;
//     OBInit.RDPLevel = OB_RDP_LEVEL_2;
//     HAL_FLASH_Unlock();
//     HAL_FLASH_OB_Unlock();
//     HAL_FLASHEx_OBProgram(&OBInit);
//     HAL_FLASH_OB_Lock();
//     HAL_FLASH_Lock();
//   }
//   __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
// }
// #define FLASH_FKEY1                          ((uint32_t)0x45670123ul)        /*!< Flash program erase key1 */
// #define FLASH_FKEY2                          ((uint32_t)0xCDEF89ABul)        /*!< Flash program erase key2: used with FLASH_PEKEY1
//                                                                                 to unlock the write access to the FPEC. */
// #define FLASH_OPTKEY11                        ((uint32_t)0x45670123ul)        /*!< Flash option key1 */
// #define FLASH_OPTKEY21                        ((uint32_t)0xCDEF89ABul)        /*!< Flash option key2: used with FLASH_OPTKEY1 to*/

// #define OB_RDP_Level_1   ((uint8_t)0xBB)
// #define OB_RDP_Level_2   ((uint8_t)0xCC)
// void FLASH_Unlock(void)
// {
//   if((FLASH->CR & FLASH_CR_LOCK) != RESET)
//   {
//     /* Unlocking the program memory access */
//     FLASH->KEYR = FLASH_FKEY1;
//     FLASH->KEYR = FLASH_FKEY2;
//   }
// }
// void FLASH_OB_Unlock(void)
// {
//   if((FLASH->CR & FLASH_CR_OPTWRE) == RESET)
//   {
//     /* Unlocking the option bytes block access */
//     FLASH->OPTKEYR = FLASH_OPTKEY11;
//     FLASH->OPTKEYR = FLASH_OPTKEY21;
//   }
// }

// void FLASH_OB_Lock(void)
// {
//   /* Set the OPTWREN Bit to lock the option bytes block access */
//   FLASH->CR &= ~FLASH_CR_OPTWRE;
// }
// #define IS_FLASH_CLEAR_FLAG(FLAG) ((((FLAG) & (uint32_t)0xFFFFFFC3) == 0x00000000) && ((FLAG) != 0x00000000))
// void FLASH_ClearFlag(uint32_t FLASH_FLAG)
// {
//   /* Check the parameters */
//   assert_param(IS_FLASH_CLEAR_FLAG(FLASH_FLAG));

//   /* Clear the flags */
//   FLASH->SR = FLASH_FLAG;
// }
// void FLASH_OB_Launch(void)
// {
//   /* Set the OBL_Launch bit to launch the option byte loading */
//   FLASH->CR |= FLASH_CR_OBL_LAUNCH;
// }
// void FLASH_Lock(void)
// {
//   /* Set the LOCK Bit to lock the FLASH control register and program memory access */
//   FLASH->CR |= FLASH_CR_LOCK;
// }

// #define  FLASH_CR_OBL_LAUNCH1                 ((uint32_t)0x00002000)
// void flash_display(void)
// {
// #if 0
//   FLASH_OBProgramInitTypeDef OBInit;
//   HAL_StatusTypeDef status = HAL_OK;

// //  __HAL_FLASH_PREFETCH_BUFFER_DISABLE();

//   HAL_FLASHEx_OBGetConfig(&OBInit);
// //  trace_printf("a\n");
// //  if(OBInit.RDPLevel == OB_RDP_LEVEL_0)
// //  {
//     if(SET != FLASH_OB_GetRDP()) {
//     OBInit.OptionType = OPTIONBYTE_RDP;
//     OBInit.RDPLevel = OB_RDP_LEVEL_2;
//     HAL_FLASH_Unlock();
//     HAL_FLASH_OB_Unlock();
// //    status = HAL_FLASHEx_OBProgram(&OBInit);
//     FLASH_OB_RDPConfig(OB_RDP_Level_1);
// #if 0
//       SET_BIT(FLASH->CR, FLASH_CR_OPTPG);

//       WRITE_REG(OB->RDP, OB_RDP_LEVEL_2);

//       /* Wait for last operation to be completed */
//       status = FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);

//       /* if the program operation is completed, disable the OPTPG Bit */
//       CLEAR_BIT(FLASH->CR, FLASH_CR_OPTPG);
// #endif
// //    HAL_FLASH_OB_Launch();

//     HAL_FLASH_OB_Lock();
//     HAL_FLASH_Lock();
//         FLASH->CR |= FLASH_CR_OBL_LAUNCH1;
// //      flash_write_data( (0x08003fd0ul), (0x00003655ul));
//     }
// //    FLASH->CR |= FLASH_CR_LOCK;
// //        HAL_FLASH_OB_Launch();
// //  }
// //  trace_printf("%d\n", status);
// //  __HAL_FLASH_PREFETCH_BUFFER_ENABLE();

// //  HAL_FLASH_OB_Launch();
// #else

//     FLASH_Unlock();

//     FLASH_OB_Unlock();

//     FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);

//     FLASH_OB_RDPConfig(OB_RDP_Level_1);
//     FLASH_OB_Lock();

// /*    FLASH_Unlock();
//     FLASH_OB_Unlock();

//             FLASH_OB_RDPConfig(OB_RDP_Level_1);

//     FLASH_OB_Launch();
//     FLASH_OB_Lock();
//     FLASH_Lock();*/

// #endif
// }