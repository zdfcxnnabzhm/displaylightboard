#include "tim.h"
#include "light_contrl.h"
#include "gpio.h"
#include "main.h"
#include "uart_app.h"
#include "usart.h"
#include "gpio.h"
#include "string.h"
#include "stm32f0xx_it.h"
#include "rtc_wake.h"

//思路是改变pwm的数值不一样，需要根据时间算出来，改变pwm的时间一样，都为10ms，以及补全pwm没有除尽的部分


static void get_data(void);
static void light_play(void);
static uint16_t pwm_count(uint16_t pwm_now, uint16_t begin_rgb, uint16_t end_rgb, uint8_t rgb_select);
static void pwm_interval(uint16_t pwm_big, uint16_t pwm_small, uint8_t time, uint16_t *interval, uint16_t *completion);
static void set_pwm(uint16_t r,uint16_t g,uint16_t b);
static void UartRxTimeOut(void);
static void FireflyPwmCompute(uint16_t *pwm_set_data,
                       uint16_t *setpwm_lack_count,
                       uint16_t *pwm_completion,
                       uint16_t rgb_select,
                       uint8_t up_or_out);
static void flicker_light_play(void);
static void LowPowerRtc(void);

static void LowPowerRtcTimeSet(uint16_t time_data);
static uint16_t LowPowerRtcTimeGet(void);


//显示模式数组                          1     1       3           3       1     1
uint8_t ring_loop_data[11] = {0};     //cmd + len + begin rgb + end rgb + time + times
//time为时间，单位为100ms，times为次数，如果为ff，则为一直显示
uint8_t pattern_end_timer = 0;        //结束的次数，0xff则一直运行
uint8_t uart_rx_timeout = 0;          //串口接收超时标志位

uint16_t begin_rgb[3] = {0};
uint16_t end_rgb[3] = {0};

uint16_t start_end_pwm[6] = {0};      //存储需要显示的pwm的最小和最大数值，用于判断是否需要显示和停止显示
uint16_t pwm_rgb[3] = {0};            //rgb转为pwm数值，除以时间后的结果
uint16_t pwm_completion[3] = {0};     //rgb转为pwm数值，除以时间后的余数，用于补全pwm

uint16_t flicker_time = 0;            //闪烁亮起的时间
uint16_t flicker_cycle_time = 0;           //闪烁的总周期
uint16_t flicker_light_need_play = 0; //开始执行闪烁的标志位
uint16_t LowRtcFlag = 0;              //闪烁灯的时候，如果rgb的pwm都为0则进入低功耗


static volatile uint32_t time_flag = 0; //时间，10ms为单位
static uint32_t TimeOver = 0;           //串口接收数据超时计时
static uint16_t flicker_timing = 0;     //闪烁灯计时
static uint16_t LowPowerRtcTime = 0;    //进入低功耗的时间


//需要设置的pwm可能除不尽，有余数，所以要补全，这个是余数的值，用于计数，是否补全了pwm
static uint16_t setpwm_r_lack_count = 0, setpwm_g_lack_count = 0, setpwm_b_lack_count = 0;

uint8_t timer_now = 0;       //模型执行的次数
uint8_t aRxBuffer;           //串口接收数据
uint8_t Uart1_RxBuff[50];    //串口存储数据
uint8_t Uart1_Rx_Cnt = 0;    //串口存储数据个数
uint8_t begin_play = 0;      //开始处理数据标位，1开始，0退出
uint8_t light_need_play = 0; //开始灯光显示

#ifndef ARRAYSIZE
#define ARRAYSIZE(a) (sizeof(a) / sizeof((a)[0]))
#endif

/*返回时间数据，单位为10ms*/
uint32_t get_task_time(void)
{
  return time_flag;
}



/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim: TIM handle
  * @retval None
  */
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
void TimingInterruptDeal(void)
{
    time_flag++;

    if(uart_rx_timeout == 1) {                //串口接收数据超时计时
      TimeOver++;
    }

    if(flicker_light_need_play == 1) {        //闪烁灯计时
      flicker_timing++;
    }

}

// 任务结构体
typedef struct{
          void (*fTask)(void);
          uint32_t uNextTick;
          uint32_t uLenTick;
}sTask;

/**
 * 建立任务，第一个是任务名，第二个数据是开机后第一次运行的时间（单位10ms），
 * 第三个数据是每次运行任务的间隔时间，（单位10ms）
 */
static sTask mTaskTab[] =
{
      {get_data,              10 ,  5},
      {light_play,            10 ,  1},
      {UartRxTimeOut,         10 ,  1},
      {flicker_light_play,    10 ,  1},
      {low_power_detect,      10 ,  20},
      {LowPowerRtc,           10 ,  1}
      ,
    // 鍦ㄨ繖涔嬪墠娣诲姞浠诲姟
};

/**
 * 任务循环
 */
int j = 0;
void light_task(void)
{
  for (j = 0; j < ARRAYSIZE(mTaskTab); j++) {
      if (mTaskTab[j].uNextTick <= get_task_time()) {
              mTaskTab[j].uNextTick += mTaskTab[j].uLenTick;
              mTaskTab[j].fTask();
      }
  }
}

/**
 * 串口接收超时标志位，如果串口没有接收到完整数据，超过时间后清空数据
 */
static void UartRxTimeOut(void)
{
//  static uint32_t TimeNow = 0;

  if(uart_rx_timeout == 0) {
    return;
  }

  if(TimeOver >= 10) {
    uart_rx_timeout = 0;
    TimeOver = 0;
    Uart1_Rx_Cnt = 0;
    memset(Uart1_RxBuff,0x00,sizeof(Uart1_RxBuff));
  }
}

/**
 * 用于计算当前灯光的pwm需要怎么设置
 * @param pwm_set_data      pwm需要设置的值
 * @param setpwm_lack_count 需要补全的pwm的数值计数
 * @param pwm_completion    需要补全的pwm的数值
 * @param rgb_select        需要补全的哪路（rgb对应0,1,2）
 * @param uint8_t           是要加（1）还是减去（0）
 */
void FireflyPwmCompute(uint16_t *pwm_set_data,
                       uint16_t *setpwm_lack_count,
                       uint16_t *pwm_completion,
                       uint16_t rgb_select,
                       uint8_t up_or_out)
{
  if(0 == up_or_out) {
    if(*setpwm_lack_count < pwm_completion[rgb_select]) {  //需要补全
      *pwm_set_data = *pwm_set_data - pwm_rgb[rgb_select] - 1;
      (*setpwm_lack_count)++;
    }else {
      *pwm_set_data = *pwm_set_data - pwm_rgb[rgb_select];             //不需要补全
    }
  }else if(1 == up_or_out) {
    if(*setpwm_lack_count < pwm_completion[rgb_select]) {  //需要补全
      *pwm_set_data = *pwm_set_data + pwm_rgb[rgb_select] + 1;
      (*setpwm_lack_count)++;
    }else {
      *pwm_set_data = *pwm_set_data + pwm_rgb[rgb_select];             //不需要补全
    }
  }
}

/**
*用于计算需要显示的pwm数值的函数，rgb_select为需要计算的是rgb其中的哪个数值，
*需要与begin_rgb和end_rgb填入的rgb一样
*/
static uint16_t color_r = 0, color_g = 0, color_b = 0;               //需要显示的pwm
uint16_t pwm_count(uint16_t pwm_now, uint16_t begin_rgb, uint16_t end_rgb, uint8_t rgb_select)
{
  uint16_t pwm_set_data = 0;
  pwm_set_data = pwm_now;

  if(begin_rgb >= end_rgb) {            //如果开始的rgb比结束的rgb大，则pwm需要一直加
    if(pwm_set_data <= begin_rgb) {        //比结束的rgb还小，还需要改pwm

      if(rgb_select == 0) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_r_lack_count, pwm_completion, rgb_select, 0);
      }else if(rgb_select == 1) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_g_lack_count, pwm_completion, rgb_select, 0);
      }else if(rgb_select == 2) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_b_lack_count, pwm_completion, rgb_select, 0);
      }
    }
  }else {
    if(pwm_set_data <= end_rgb) {        //比结束的rgb还大，还需要改pwm

      if(rgb_select == 0) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_r_lack_count, pwm_completion, rgb_select, 1);
      }else if(rgb_select == 1) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_g_lack_count, pwm_completion, rgb_select, 1);
      }else if(rgb_select == 2) {
        FireflyPwmCompute(&pwm_set_data, &setpwm_b_lack_count, pwm_completion, rgb_select, 1);
      }
    }
  }
  return pwm_set_data;
}

//呼吸灯，pwm开始显示
static uint8_t light_flag = 0;  //灯光一个周期分两个部分 0 1，反过来

void light_play(void)
{
  if(light_need_play == 0) {
    return ;
  }

  if(light_flag == 0) {

    color_r = pwm_count(color_r, start_end_pwm[0], start_end_pwm[3], 0);     //获取数值
    color_g = pwm_count(color_g, start_end_pwm[1], start_end_pwm[4], 1);
    color_b = pwm_count(color_b, start_end_pwm[2], start_end_pwm[5], 2);

  }else if(light_flag == 1) {
    color_r = pwm_count(color_r, start_end_pwm[3], start_end_pwm[0], 0);     //获取数值
    color_g = pwm_count(color_g, start_end_pwm[4], start_end_pwm[1], 1);
    color_b = pwm_count(color_b, start_end_pwm[5], start_end_pwm[2], 2);

  }
  set_pwm(color_r, color_g, color_b);         //显示pwm

  if(color_r == start_end_pwm[3] && color_g == start_end_pwm[4] && color_b == start_end_pwm[5] && light_flag == 0) {
    setpwm_r_lack_count = 0;           //补全pwm的标志，需要清0
    setpwm_g_lack_count = 0;
    setpwm_b_lack_count = 0;
    light_flag = 1;                                               //pwm需要++还是--
  }else if(color_r == start_end_pwm[0] && color_g == start_end_pwm[1] && color_b == start_end_pwm[2] && light_flag == 1) {
    setpwm_r_lack_count = 0;           //补全pwm的标志，需要清0
    setpwm_g_lack_count = 0;
    setpwm_b_lack_count = 0;
    light_flag = 0;

    timer_now++;
  }

  if(timer_now >= pattern_end_timer && pattern_end_timer != 0xff) {
    light_need_play = 0;
  }

}

//闪烁灯模式
void flicker_light_play(void)
{
  //为了避免pwm为0的时候进入低功耗，然后刚退出低功耗的时候pwm还是0，马上又进入低功耗，死循环，所以初始值都为1
  uint16_t pwm_r = 1, pwm_g = 1, pwm_b = 1;
  static uint16_t time_remaining = 0;
  if(flicker_light_need_play == 0) {
    return ;
  }

  if(flicker_timing <= flicker_time) {                                              //亮begin rgb
    pwm_r = begin_rgb[0] * 4;
    pwm_g = begin_rgb[1] * 4;
    pwm_b = begin_rgb[2] * 4;

    set_pwm(pwm_r, pwm_g, pwm_b);
    time_remaining = flicker_time - flicker_timing;                                 //获取需要进入低功耗的时间
  }else if(flicker_timing > flicker_time && flicker_timing < flicker_cycle_time) {  //亮end rgb
    pwm_r = end_rgb[0] * 4;
    pwm_g = end_rgb[1] * 4;
    pwm_b = end_rgb[2] * 4;

    set_pwm(pwm_r, pwm_g, pwm_b);
    time_remaining = flicker_cycle_time - flicker_timing;                           //获取需要进入低功耗的时间
  }else if(flicker_timing > flicker_cycle_time) {                                   //一个周期结束，重新来
    flicker_timing = 0;
    timer_now++;
  }

  if(pwm_r == 0 && pwm_g == 0 && pwm_b == 0) {                                      //如果都为0则进入低功耗
    LowRtcFlag = 1;
    LowPowerRtcTimeSet(time_remaining);
  }

  if(timer_now >= pattern_end_timer && pattern_end_timer != 0xff) {                 //闪烁的次数
    flicker_timing = 0;
    flicker_light_need_play = 0;
  }

}


/**
 * 计算每次pwm需要改变的数值（大的pwm减去小的pwm，除以时间）
 * 和需要补全的数值（大的pwm减去小的pwm，除以时间得到的余数）
 * @param pwm_big    [description]
 * @param pwm_small  [description]
 * @param time       串口获取到的时间数据，单位为100ms，所以总时间为time*100，
 *                   又因为固定每10ms改变一次pwm，所以time*100/100，即(time_pwm * 10)
 * @param interval   每10ms改变的pwm数值
 * @param completion 需要补全的pwm数值
 */
void pwm_interval(uint16_t pwm_big, uint16_t pwm_small, uint8_t time, uint16_t *interval, uint16_t *completion)
{
  uint16_t temp = 0;
  uint16_t time_pwm = (uint16_t)time;

  temp = (pwm_big - pwm_small) / (time_pwm * 10);
  *interval = temp;                                            //pwm变化数值

  temp = (pwm_big - pwm_small) % (time_pwm * 10);
  *completion = temp;                                          //pwm需要补全的数值
}


/**
 * [firefly_rgb_deal description]
 * @param begin 模式开始时候的rgb
 * @param end   模式结束时候的rgb
 * @param time  模式的一个周期时间
*/
void firefly_rgb_deal(uint16_t *begin, uint16_t *end, uint8_t time)
{
  uint8_t i = 0;

  for(i = 0; i < 3; i++) {            //获取最小的pwm数值
    start_end_pwm[i] = begin[i] * 4;
  }

  for(i = 0; i < 3; i++) {            //获取最大的pwm数值
    start_end_pwm[3 + i] = end[i] * 4;
  }

  for(i = 0; i < 3; i++) {                          //获取每10ms需要改变的pwm数值,以及需要补全的pwm
    if(start_end_pwm[i] > start_end_pwm[3 + i]) {
      pwm_interval(start_end_pwm[i],
                   start_end_pwm[3 + i],
                   (uint16_t)time,
                   &pwm_rgb[i],
                   &pwm_completion[i]);
    }else {
      pwm_interval(start_end_pwm[3 + i],
                   start_end_pwm[i],
                   (uint16_t)time,
                   &pwm_rgb[i],
                   &pwm_completion[i]);
    }

  }

}

//数据接收完，开始处理数据
void get_data(void)
{
  if(begin_play == 0) {
    return;
  }

  uint8_t i = 0;
  if(ring_loop_data[0] == BREATHE_FLAG) {               //呼吸灯
    for(i = 0; i < 3; i++) {
      begin_rgb[i] = ring_loop_data[2 + i];             //开始rgb
      end_rgb[i] = ring_loop_data[5 + i];               //结束的rgb
    }
    pattern_end_timer = ring_loop_data[9];             //执行次数

    firefly_rgb_deal(begin_rgb, end_rgb, ring_loop_data[8]);     //处理rgb为 需要显示的pwm数值

    light_need_play = 1;
    flicker_light_need_play = 0;
  }else if(ring_loop_data[0] == FLICKER_FLAG) {         //闪烁灯
    for(i = 0; i < 3; i++) {
      begin_rgb[i] = ring_loop_data[2 + i];             //开始rgb
      end_rgb[i] = ring_loop_data[5 + i];               //结束的rgb
    }

      pattern_end_timer = ring_loop_data[10];             //执行次数
      flicker_cycle_time = ring_loop_data[9] * 10;             //周期时间
      flicker_time = flicker_cycle_time * ring_loop_data[8] / 100;//需要亮多久

/*      uart_set(&flicker_cycle_time, 1);
      uart_set(&flicker_time, 1);*/

      flicker_light_need_play = 1;
      light_need_play = 0;
  }

  setpwm_r_lack_count = 0;           //补全pwm的标志，需要清0
  setpwm_g_lack_count = 0;
  setpwm_b_lack_count = 0;

  color_r = start_end_pwm[0], color_g = start_end_pwm[1], color_b = start_end_pwm[2];

  timer_now = 0;
  light_flag = 0;
  begin_play = 0;
  flicker_timing = 0;
  LowRtcFlag = 0;
}


//发送pwm数据
void set_pwm(uint16_t r,uint16_t g,uint16_t b)
{
  LL_TIM_OC_SetCompareCH1(TIM16, r);
  LL_TIM_OC_SetCompareCH1(TIM14, g);
  LL_TIM_OC_SetCompareCH1(TIM1, b);
}


/**
  * @brief  Rx Transfer completed callbacks.
  * @param  huart pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
//先判断是否接收到正常长度的数据，在判断数据是是否正确
#define UART_RX_OVERFLOW        (50)
void USART1_IRQHandler_simulation(void)
{
  if(LL_USART_IsActiveFlag_RXNE(USART1)) {
    if(Uart1_Rx_Cnt >= UART_RX_OVERFLOW)  {             //判断是否溢出
      Uart1_Rx_Cnt = 0;
      memset(Uart1_RxBuff, 0x00, sizeof(Uart1_RxBuff));
    }else {
      Uart1_RxBuff[Uart1_Rx_Cnt++] = LL_USART_ReceiveData8(USART1);     //接收数据转存

      if(Uart1_Rx_Cnt >= COM_LEN)  {                        //如果接收到的数据超过11个说明有指令过来，暂时
          if(Uart1_RxBuff[0] == UART_BEGIN_HEARD) {         //heard头是正确的则进入数据处理阶段
            if(Uart1_Rx_Cnt - 1 >= Uart1_RxBuff[2]) {       //数据长度为正确的开始处理(数据长度没有算头，所以要减1)
              for(int i = 0; i < Uart1_Rx_Cnt - 1; i++) {
                ring_loop_data[i] = Uart1_RxBuff[1 + i];    //只从rgb开始拿
              }
              begin_play = 1;
              Uart1_Rx_Cnt = 0;
              memset(Uart1_RxBuff,0x00,sizeof(Uart1_RxBuff));
            }
          }else {
            memset(Uart1_RxBuff,0x00,sizeof(Uart1_RxBuff)); //接收完清空数组
          }
          uart_rx_timeout = 0;
          TimeOver = 0;       //数据接收正确，去掉清理数据的标志
      }
      uart_rx_timeout = 1;    //数据接收错误，100ms后清掉数据
    }
  }
}

/**
 * 检测到空闲则进入低功耗模式,退出低功耗之后需要200ms才能再次进入低功耗
 */
uint8_t testt = 0;
void low_power_detect(void)
{
  if(begin_play != 0 ||
     flicker_light_need_play != 0 ||
     light_need_play != 0 ||
     uart_rx_timeout != 0) {
    return ;
  }

  stop_mode_wake_pin_modify();

  PwrStopEntry();
  power_off_detection_pin_setting();
}

/**
 * 获取需要进入低功耗的时间
 * @param time_data 时间数值，单位为10ms
 */
void LowPowerRtcTimeSet(uint16_t time_data)
{
  LowPowerRtcTime = time_data;
}

/**
 * 获取需要进入低功耗的时间
 * @return           将时间单位返回
 */
uint16_t LowPowerRtcTimeGet(void)
{
  return LowPowerRtcTime;
}

/**
 * 进入RTC和中断都可以唤醒的低功耗
 */
void LowPowerRtc(void)
{
  if(LowRtcFlag != 1) {
    return ;
  }

  stop_mode_wake_pin_modify();
  RtcWakeSet(LowPowerRtcTimeGet());
//  RtcWakeSet(100);

  PwrStopEntry();
  power_off_detection_pin_setting();
  flicker_timing = flicker_cycle_time;
  LowRtcFlag = 0;
}
