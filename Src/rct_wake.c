#include "tim.h"
#include "light_contrl.h"
#include "gpio.h"
#include "main.h"
#include "uart_app.h"
#include "usart.h"
#include "gpio.h"
#include "string.h"
#include "rtc.h"
#include "stm32f0xx_it.h"
#include "rtc_wake.h"


static void RtcWakeTime(uint8_t hou_data, uint8_t min_data, uint8_t sec_data);

/**
 * rtc中断exti17配置
 */
void rtc_interrupt_init(void)
{
  LL_EXTI_InitTypeDef EXTI_InitStruct = {0};

  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_17);
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_17);           //开启中断

  EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_11;
  EXTI_InitStruct.LineCommand = ENABLE;
  EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
  EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING_FALLING; //触发升降模式
  LL_EXTI_Init(&EXTI_InitStruct);
}

/**
 * 配置rtc中断优先级
 */
void rtc_interrupt_priority(void)
{

}

/**
 * 设置RTC时钟的时间
 * @param time_data 单位为10ms
 */
void RtcWakeTimeSet(uint16_t time_data)
{
  uint8_t hour = 0, minutes = 0, seconds = 0;
  hour = (time_data / 3600) & 0xff;
  minutes = ((time_data % 3600) / 60) & 0xff;
  seconds = ((time_data % 3600) % 60) & 0xff;

  RtcWakeTime(hour, minutes, seconds);
}

/**
 * 配置rtc中断唤醒时间，每一秒时间为10ms
 * @param min_data 设置分钟数
 * @param sec_data 设置秒数
 */
void RtcWakeTime(uint8_t hou_data, uint8_t min_data, uint8_t sec_data)
{
  LL_RTC_AlarmTypeDef RTC_AlarmStruct = {0};

  MX_RTC_Init();

//设置唤醒时间，分钟和秒都需要匹配才唤醒，每一秒实际为10ms
  RTC_AlarmStruct.AlarmTime.Hours   =  hou_data;
  RTC_AlarmStruct.AlarmTime.Minutes =  min_data;
  RTC_AlarmStruct.AlarmTime.Seconds =  sec_data;
  RTC_AlarmStruct.AlarmMask = LL_RTC_ALMA_MASK_DATEWEEKDAY;//LL_RTC_ALMA_MASK_NONE|LL_RTC_ALMA_MASK_MINUTES;
  LL_RTC_ALMA_Init(RTC,LL_RTC_FORMAT_BIN,&RTC_AlarmStruct);
  LL_RTC_DisableWriteProtection(RTC);                                     //关闭写保护
  LL_RTC_ALMA_Enable(RTC);
  LL_RTC_ClearFlag_ALRA(RTC);
  LL_RTC_EnableIT_ALRA(RTC);
  LL_RTC_EnableWriteProtection(RTC);                                      //开启写保护
}


/**
 * 清除RTC唤醒stop模式后的中断位标志（在rtc中断里)，不清除的话，唤醒后退不出中断，一直在中断里
 */
void ClearWfeInterFlag(void)
{
  LL_RTC_DisableWriteProtection(RTC);             //禁用rtc写保护
  LL_RTC_ALMA_Disable(RTC);                       //禁用警报标志
  LL_RTC_ClearFlag_ALRA(RTC);                     //清除警报标志
  LL_RTC_EnableWriteProtection(RTC);              //启用rtc写保护
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_17);        //清除exti警报标志
}

/*void GetRtcTime(void)
{
  LL_RTC_TIME_GetMinute();
  LL_RTC_TIME_GetSecond();
}*/



