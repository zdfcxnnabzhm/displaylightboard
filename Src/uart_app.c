#include "usart.h"
#include "uart_app.h"
#include "rtc_wake.h"
#include "gpio.h"
#include "tim.h"
#include "main.h"
#include "rtc.h"
#include "light_contrl.h"
#include "stm32f0xx_ll_rtc.h"
#include "stm32f0xx_ll_exti.h"
#include "stm32f0xx_hal_rtc.h"
#include "stm32f0xx_ll_pwr.h"
#include "drive_set.h"

static void exit_uart_rx(void);
static void uart_down(void);

/**
 * 串口发送数据
 * @param temp1 要发送的字符串
 * @param len   字符串长度
 */
void uart_set(uint16_t *temp1, uint16_t len)
{
  uint16_t DelayUartSend = 0;
  uint16_t fre = 0;
  uint8_t uart_data = 0;

  for(fre = 0; fre < len; fre++) {
    uart_data = ((temp1[fre] >> 8) & 0xff);
    LL_USART_TransmitData8(USART1, uart_data);
    //串口发送完成后才发送新的数据，否则会只发送上次的数据，防止卡死
    while(!LL_USART_IsActiveFlag_TXE(USART1) || DelayUartSend >=  1000){
      DelayUartSend++;
    }
    uart_data = (temp1[fre] & 0xff);
    LL_USART_TransmitData8(USART1, uart_data);
    DelayUartSend = 0;
  }
}


//开启接收中断
void UartInterruptInit(void)
{
  LL_USART_EnableIT_RXNE(USART1);
  LL_USART_EnableIT_PE(USART1);
}
//关闭串口中断
void uart_down(void)
{
  LL_USART_DisableIT_RXNE(USART1);
  LL_USART_DisableIT_PE(USART1);
}

#define PWR_MAINREGULATOR_ON            (0x00000000U)
#define PWR_LOWPOWERREGULATOR_ON        PWR_CR_LPDS
#define IS_PWR_REGULATOR(REGULATOR) (((REGULATOR) == PWR_MAINREGULATOR_ON) || \
                                     ((REGULATOR) == PWR_LOWPOWERREGULATOR_ON))

#define PWR_STOPENTRY_WFI               ((uint8_t)0x01U)
#define PWR_STOPENTRY_WFE               ((uint8_t)0x02U)
#define IS_PWR_STOP_ENTRY(ENTRY) (((ENTRY) == PWR_STOPENTRY_WFI) || ((ENTRY) == PWR_STOPENTRY_WFE))


//进入低功耗
void PwrStopEntry(void)
{
  uint32_t tmpreg = 0;

  assert_param(IS_PWR_REGULATOR(PWR_CR_LPDS));
  assert_param(IS_PWR_STOP_ENTRY(((uint8_t)0x01U)));

  tmpreg = PWR->CR;
  tmpreg &= (uint32_t)~(PWR_CR_PDDS | PWR_CR_LPDS);
  tmpreg |= PWR_LOWPOWERREGULATOR_ON;
  PWR->CR = tmpreg;

  SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

#if 0
  __WFI();
#else
  __SEV();
  __WFE();
  __WFE();
#endif

  SCB->SCR &= (uint32_t)~((uint32_t)SCB_SCR_SLEEPDEEP_Msk);
}


//进入低功耗模式前设置引脚属性
void stop_mode_wake_pin_modify(void)
{
  exit_uart_rx();
//    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
}


/**
 * 设置rtc唤醒时间
 * @param wake_time 单位10ms
 */
void RtcWakeSet(uint16_t wake_time)
{
  rtc_interrupt_init();
  RtcWakeTimeSet(wake_time);
}

//退出低功耗模式
void power_off_detection_pin_setting(void)
{
  SystemClock_Config();           //使能时钟

  user_tim3Init();                //开启时钟

  MX_USART1_UART_Init();          //开启串口
  UartInterruptInit();            //使能串口中断

  //pwm初始化
  MX_TIM16_Init();
  MX_TIM1_Init();
  MX_TIM14_Init();

  pwm_init();
}

//设置串口引脚为退出低功耗的引脚
void exit_uart_rx(void)
{
  LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
/*  GPIO_InitTypeDef GPIO_InitStruct1 = {0};
  GPIO_InitTypeDef GPIO_InitStruct2 = {0};
  GPIO_InitTypeDef GPIO_InitStruct3 = {0};*/
  LL_GPIO_InitTypeDef GPIO_InitStruct_Pwm = {0};
//关闭pwm
  pwm_down();
//关闭串口时钟
/*  LL_APB1_GRP2_DisableClock(LL_APB1_GRP2_PERIPH_USART1);
  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOA);*/
  uart_down();
  user_tim3Stop();

  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

  GPIO_InitStruct_Pwm.Pin = LL_GPIO_PIN_3;
  GPIO_InitStruct_Pwm.Mode = LL_GPIO_MODE_INPUT;
  GPIO_InitStruct_Pwm.Pull = LL_GPIO_PULL_DOWN;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct_Pwm);

  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_3);
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_3);           //开启中断

  EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_3;
  EXTI_InitStruct.LineCommand = ENABLE;
  EXTI_InitStruct.Mode = LL_EXTI_MODE_EVENT;
  EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING_FALLING; //触发升降模式
  LL_EXTI_Init(&EXTI_InitStruct);

  /*Configure GPIO pin : PA3 */
/*  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;     //设置为evt退出低功耗
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);*/
#if 0
  GPIO_InitStruct1.Pin = GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct1.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct1.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct1);


  GPIO_InitStruct1.Pin = GPIO_PIN_2;
  GPIO_InitStruct1.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct1.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct3);

  GPIO_InitStruct2.Pin = GPIO_PIN_1;
  GPIO_InitStruct2.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct2.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct2);
#else

  GPIO_InitStruct_Pwm.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct_Pwm.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct_Pwm.Pull = LL_GPIO_PULL_DOWN;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct_Pwm);

  GPIO_InitStruct_Pwm.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct_Pwm.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct_Pwm.Pull = LL_GPIO_PULL_DOWN;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct_Pwm);

  GPIO_InitStruct_Pwm.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct_Pwm.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct_Pwm.Pull = LL_GPIO_PULL_DOWN;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct_Pwm);

  GPIO_InitStruct_Pwm.Pin = LL_GPIO_PIN_1;
  GPIO_InitStruct_Pwm.Mode = LL_GPIO_MODE_ANALOG;
  GPIO_InitStruct_Pwm.Pull = LL_GPIO_PULL_DOWN;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct_Pwm);
#endif
}

#if 0
void RccClockInternal(void)
{
//改为内部晶振，不用外部
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_1);

  if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_1)
  {
  Error_Handler();
  }
  LL_RCC_HSI_Enable();

   /* Wait till HSI is ready */
  while(LL_RCC_HSI_IsReady() != 1)
  {

  }
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_LSI_Enable();

   /* Wait till LSI is ready */
  while(LL_RCC_LSI_IsReady() != 1)
  {

  }
  LL_PWR_EnableBkUpAccess();
  LL_RCC_ForceBackupDomainReset();
  LL_RCC_ReleaseBackupDomainReset();
  LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSI);
  LL_RCC_EnableRTC();
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_8);
  LL_RCC_PLL_Enable();

   /* Wait till PLL is ready */
  while(LL_RCC_PLL_IsReady() != 1)
  {

  }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {

  }
  LL_Init1msTick(32000000);
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  LL_SetSystemCoreClock(32000000);
  LL_RCC_SetUSARTClockSource(LL_RCC_USART1_CLKSOURCE_HSI);

}

#endif



