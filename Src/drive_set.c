#include "tim.h"
#include "light_contrl.h"
#include "gpio.h"
#include "main.h"
#include "uart_app.h"
#include "usart.h"
#include "gpio.h"
#include "string.h"
#include "stm32f0xx_it.h"
#include "rtc_wake.h"
#include "drive_set.h"


//关闭pwm
void pwm_down(void)
{
  LL_TIM_CC_DisableChannel(TIM16, LL_TIM_CHANNEL_CH1);
  LL_TIM_DisableCounter(TIM16);

  LL_TIM_CC_DisableChannel(TIM1, LL_TIM_CHANNEL_CH1);
  LL_TIM_DisableCounter(TIM1);

  LL_TIM_CC_DisableChannel(TIM16, LL_TIM_CHANNEL_CH1);
  LL_TIM_DisableCounter(TIM16);

  LL_TIM_DisableAllOutputs(TIM1);                        //开启输出
  LL_TIM_DisableAllOutputs(TIM14);
  LL_TIM_DisableAllOutputs(TIM16);
}

//开启pwm
void pwm_init(void)
{
  //使能通道，注意这里TIM1的通道是LL_TIM_CHANNEL_CH1N，对应stm32Cubemx上的通道
  LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH1N);
  LL_TIM_EnableCounter(TIM1);

  LL_TIM_CC_EnableChannel(TIM14, LL_TIM_CHANNEL_CH1);
  LL_TIM_EnableCounter(TIM14);

  LL_TIM_CC_EnableChannel(TIM16, LL_TIM_CHANNEL_CH1);
  LL_TIM_EnableCounter(TIM16);

  LL_TIM_EnableAllOutputs(TIM1);                        //开启输出
  LL_TIM_EnableAllOutputs(TIM14);
  LL_TIM_EnableAllOutputs(TIM16);
}

//开启定时器3
void user_tim3Init(void)
{
  LL_TIM_EnableIT_UPDATE(TIM3);
  LL_TIM_EnableCounter(TIM3);
}

//关闭定时器3
void user_tim3Stop(void)
{
  LL_TIM_DisableIT_UPDATE(TIM3);
  LL_TIM_DisableCounter(TIM3);
}