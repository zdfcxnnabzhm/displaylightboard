#ifndef _UART_APP_H_
#define _UART_APP_H_

void UartInterruptInit(void);
void stop_mode_wake_pin_modify(void);
void power_off_detection_pin_setting(void);
void uart_set(uint16_t *temp1, uint16_t len);

void PwrStopEntry(void);
void RtcWakeSet(uint16_t wake_time);

#endif