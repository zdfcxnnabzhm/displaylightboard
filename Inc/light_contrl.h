#ifndef _LIGHT_CONTRL_H_
#define _LIGHT_CONTRL_H_

void led_stage_light_time(void);
void light_task(void);
uint32_t get_task_time(void);
void low_power_detect(void);
void USART1_IRQHandler_simulation(void);

void TimingInterruptDeal(void);


#define UART_BEGIN_HEARD		(0xf5)		//数据头，判断是否正确
#define COM_LEN	(11)

#define BREATHE_FLAG	(0x01)
#define FLICKER_FLAG	(0x02)


#endif //_LIGHT_CONTRL_H_