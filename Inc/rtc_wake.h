#ifndef _RTC_WAKE_H_
#define _RTC_WAKE_H_

void rtc_interrupt_init(void);
void rtc_interrupt_priority(void);
void RtcWakeTimeSet(uint16_t time_data);
void uart_test(void);
void ClearWfeInterFlag(void);

#endif