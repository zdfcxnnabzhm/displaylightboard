#ifndef _FLASH_H_
#define _FLASH_H_


void flash_write_data(uint32_t flash_addr, uint32_t write_flash_data);
uint32_t flash_read_data(uint32_t flash_addr);
void flash_protection(void);
void flash_play(void);
void flash_display(void);
#endif // _FLASH_H_